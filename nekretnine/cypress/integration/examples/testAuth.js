describe("test", ()=>{
    // ucitavanje slike na naslovnoj stranici
    it("render stranice",()=>{
            cy.visit("/");
            cy.get(".slika").should("exist");
        });

    // registracija korisnika
    it('registracijaKorisnika', function() {
        cy.visit('http://localhost:8080/register');
        cy.get('[href="/Register"]').click();
        cy.get('[type="email"]').clear();
           cy.get('[type="email"]').type('tina.capan@gmail.com');
           cy.get('[type="password"]').clear();
           cy.get('[type="password"]').type('tinatina');
           cy.get('button').click();
           cy.get('.container').click();
           cy.get('[type="email"]').clear();
           cy.get('[type="email"]').type('capanjurisic@gmail.com');
           cy.get('[type="password"]').click();
           cy.get('button').click();
    });

    // prijava korisnika
    it('login', function() {
        cy.visit('localhost:8080/login');
        cy.get(':nth-child(1) > p > .input').clear();
        cy.get(':nth-child(1) > p > .input').type('tina.capan@gmail.com');
        cy.get(':nth-child(2) > p > .input').clear();
        cy.get(':nth-child(2) > p > .input').type('tinatina');
        cy.get('.prijava').click()
        });

    // zaboravljena lozinka
    it('forgetPassword', function() {
            cy.visit('localhost:8080/forgetpassword');
            cy.get('.form-control').clear();
            cy.get('.form-control').type('tina.capan@gmail.com');
            cy.get('.btn').click();
        });

    
});
