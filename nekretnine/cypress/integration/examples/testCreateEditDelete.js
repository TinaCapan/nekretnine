describe("test", ()=>{
    // unos nekretnine
    it('unosNekretnine', function() {
        cy.visit('localhost:8080/listNekretnine');
        cy.get('[href="/listNekretnine"]').click();
        cy.get('.add').click();
        cy.get(':nth-child(1) > .input').clear();
        cy.get(':nth-child(1) > .input').type('Pamići 1, 52404 Sveti Petar u Šumi');
        cy.get(':nth-child(2) > .input').clear();
        cy.get(':nth-child(2) > .input').type('200000€');
        cy.get(':nth-child(3) > .input').clear();
        cy.get(':nth-child(3) > .input').type('2003.');
        cy.get(':nth-child(4) > .input').clear();
        cy.get(':nth-child(4) > .input').type('Sveti Petar u Šumi');
        cy.get('#prvi').type('da');
        cy.get('#prvi').check();
        cy.get(':nth-child(6) > .input').clear();
        cy.get(':nth-child(6) > .input').type('150m2');
        cy.get('.is-focused').select('Stan');
        cy.get('.is-focused').select('Kuca');
        cy.get(':nth-child(2) > .input').clear();
        cy.get(':nth-child(2) > .input').type('290000€');
        cy.get('#file').click();
        cy.get('.unesi').click();
        });

    // edit nekretnine
    it('editNekretnine', function() {
        cy.visit('localhost:8080/');
        cy.get('[href="/listNekretnine"]').click();
        cy.get(':nth-child(2) > .has-text-centered > a > .edit').click();
        cy.get(':nth-child(1) > .input').clear();
        cy.get(':nth-child(1) > .input').type('120000€');
        cy.get('.btn').click();
    });

    // brisanje nekretnine
    it('deleteNekretnine', function() {
        cy.visit('localhost:8080/');
        cy.get('[href="/listNekretnine"]').click();
        cy.get(':nth-child(3) > .has-text-centered > :nth-child(2)').click();
    });
});
