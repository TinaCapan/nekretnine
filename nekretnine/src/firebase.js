import { onUnmounted, ref } from '@vue/runtime-core'
import firebase from 'firebase'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: 'AIzaSyD9ltuy6Mo9YMMZZBSCtxxHNZJK61eH33Q',
  authDomain: 'nekretnine-2d786.firebaseapp.com',
  databaseURL: 'https://nekretnine-2d786-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'nekretnine-2d786',
  storageBucket: 'nekretnine-2d786.appspot.com',
  messagingSenderId: '631857664169',
  appId: '1:631857664169:web:1075a229c0013f6f9599b8',
  measurementId: 'G-Q6FMJHY46L'
}
firebase.initializeApp(firebaseConfig)

export const db = firebase.firestore()
export const nekretnineCollection = db.collection('nekretnine')
export const storage = firebase.storage()

export const createNekretnina = nekretnina => {
  return nekretnineCollection.add(nekretnina)
}

export const getNekretnina = async id => {
  const nekretnina = await nekretnineCollection.doc(id).get()
  return nekretnina.exists ? nekretnina.data() : null
}

export const updateNekretnina = (id, nekretnina) => {
  return nekretnineCollection.doc(id).update(nekretnina)
}

export const deleteNekretnina = id => {
  return nekretnineCollection.doc(id).delete()
}

export const useLoadNekretnine = () => {
  const nekretnine = ref([])
  const close = nekretnineCollection.onSnapshot(snapshot => {
    nekretnine.value = snapshot.docs.map(doc => ({ id: doc.id, ...doc.data() }))
  })
  onUnmounted(close)
  return nekretnine
}
