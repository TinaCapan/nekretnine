import { createRouter, createWebHistory } from 'vue-router'
import Edit from '@/components/Edit.vue'
import Home from '@/components/Home.vue'
import Register from '@/components/Register.vue'
import Login from '@/components/Login.vue'
import Map from '@/components/map.vue'
import ForgetPassword from '@/components/ForgetPassword'
import listNekretnine from '../components/listNekretnine'
import createNekretnine from '../components/createNekretnine'
import firebase from 'firebase'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/map',
    name: 'Map',
    component: Map
  },
  {
    path: '/register',
    name: 'Register',
    component: Register,
    meta: {
      authRequired: true
    }
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/forgetpassword',
    name: 'ForgetPassword',
    component: ForgetPassword
  },
  {
    path: '/edit/:id',
    name: 'Edit',
    component: Edit,
    meta: {
      authRequired: true
    }
  },
  {
    path: '/listNekretnine',
    name: 'Lista nekretnina',
    component: listNekretnine,
    meta: {
      authRequired: true
    }
  },
  {
    path: '/createNekretnine',
    name: 'Unos nekretnina',
    component: createNekretnine,
    meta: {
      authRequired: true
    }
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.authRequired)) {
    if (firebase.auth().currentUser) {
      next()
    } else {
      alert('Za pristup ovoj stranici morate biti prijavljeni')
      next({
        path: '/'
      })
    }
  } else {
    next()
  }
})

export default router
