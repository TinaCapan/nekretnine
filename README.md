Sustav je namijenjen za pregled nekretnina koje se prodaju.
Prilikom ulaska u aplikaciju, korisniku se prikazuje naslovna stranica sa osnovnim podacima o agenciji za nekretnine. 
Odabirom prikaza na mapi, od korisnika se traži odobrenje preuzimanja lokacije nakon čega se označi njegova lokacija na mapi sa svim nekretninama koje su na prodaju. Korisnik ima mogućnost unosa željenog radijusa kako bi me se 'zatamnile' odnosno krugom u odabranom radijusu prikazale nekretnine. Nakon odabira neke od nekretnina otvara se prozor sa informacijama o nekretnini (id nekretnine, adresa, cijena, kvadratura, tip nekretnine, cijena i pripadajuća slika).
Svaki zaposlenik unutar agencije za nekretnine ima svoj e-mail i lozinku s kojima se prijavljuje u sustav. Nakon što se prijavi, otvara mu se mogućnost pregleda nekretnina i mogućnost registriranja novih korisnika. kako bi se korisnik registrirao, potrebno je unijeti njegovu e-mail adresu i pripadajuću lozinku s kojima će se prijavljivati u sustav.
Zaposlenici imaju mogućnost pregleda nekretnina u tabličnom prikazu i mogućnost unosa novih nekretnina i mogućnost izmjene i brisanja postojećih.
Svrha izrade ove aplikacije je olakšati kupcima nekretnina pregled ponude sistematizirajući prodajne nekretnine na jednom mjestu s obzirom na lokaciju korisnika.

INSTALACIJA 
- prvo je potrebno preuzeti node.js sa službene stranice (https://nodejs.org/en/) i instalirati ga 

Instalacija na klijentskoj strani
1. korak
Otvoriti novi terminal i napisati: cd nekretnine

2. korak
U terminal napisati npm install @fawmi/vue-google-maps @googlemaps/js-api-loader axios core-js firebase vue vue-nav-ui vue-router

3. korak
Kroz terminal pokrenuti aplikaciju: npm run serve

4. korak
Otvoriti aplikaciju u pregledniku: http://localhost:8080/ 
